﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.Example.OrmTool;

namespace Solomon.Example.Web.Test
{
    [TestClass]
    public class CustomOrmToolTest
    {
        [TestMethod]
        public void OrmToolGetAllCustomers()
        {
            var customers = DbHelper.GetObjectSet<Customer>("select * from [SalesLT].[Customer]");

            Assert.IsNotNull(customers);
        }
    }
}
