﻿using Ninject;
using Solomon.Example.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Example.Web.Test
{
    public class TestBase
    {
        private IKernel kernel;
        public TestBase()
        {
            kernel = NinjectTestCommon.GetKernel();
            AutoMapperServiceConfiguration.Configure();
        }

        public IKernel Kernel
        {
            get
            {
                if (kernel == null)
                {
                    kernel = NinjectTestCommon.GetKernel();
                }
                return kernel;
            }
        }
    }
}
