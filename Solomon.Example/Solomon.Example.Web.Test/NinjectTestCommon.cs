﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Extensions.Conventions;
using Ninject;
using Solomon.Common.Repository;
using System.Data.Entity;
using Solomon.Example.DataModel;

namespace Solomon.Example.Web.Test
{
    public class NinjectTestCommon
    {
        private static IKernel kernel;

        private NinjectTestCommon() { }
        
        public static IKernel GetKernel()
        {
            if (kernel == null)
            {
                kernel = new StandardKernel(new ServiceModule());
            }

            return kernel;
        }
    }

    public class ServiceModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind(x =>
                {
                    x.FromAssemblyContaining<Solomon.Example.Services.CustomerService>()
                        .SelectAllClasses()
                        .BindDefaultInterface();
                });

            Kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            Kernel.Bind<DbContext>().To<AdventureWorksEntities>();

            Kernel.Bind(x =>
            {
                x.FromAssemblyContaining<Solomon.Example.Components.CustomerManager>()
                 .SelectAllClasses()
                 .BindDefaultInterface();
            });
        }
    }
}
