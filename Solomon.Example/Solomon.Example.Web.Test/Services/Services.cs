﻿



// ------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version: 11.0.0.0
//  
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
// ------------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

using Solomon.Example.Services;


namespace Solomon.Example.Web.Test.Services
{
    [TestClass]
    public partial class AddressServiceTest : TestBase
    {
        [TestMethod]
        public void AddressServiceGetAll()
        {
            var service = Kernel.Get<AddressService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void AddressServiceGetById()
        {
            var service = Kernel.Get<AddressService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class CustomerServiceTest : TestBase
    {
        [TestMethod]
        public void CustomerServiceGetAll()
        {
            var service = Kernel.Get<CustomerService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void CustomerServiceGetById()
        {
            var service = Kernel.Get<CustomerService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class ErrorLogServiceTest : TestBase
    {
        [TestMethod]
        public void ErrorLogServiceGetAll()
        {
            var service = Kernel.Get<ErrorLogService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void ErrorLogServiceGetById()
        {
            var service = Kernel.Get<ErrorLogService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class ProductServiceTest : TestBase
    {
        [TestMethod]
        public void ProductServiceGetAll()
        {
            var service = Kernel.Get<ProductService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void ProductServiceGetById()
        {
            var service = Kernel.Get<ProductService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class ProductCategoryServiceTest : TestBase
    {
        [TestMethod]
        public void ProductCategoryServiceGetAll()
        {
            var service = Kernel.Get<ProductCategoryService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void ProductCategoryServiceGetById()
        {
            var service = Kernel.Get<ProductCategoryService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class ProductDescriptionServiceTest : TestBase
    {
        [TestMethod]
        public void ProductDescriptionServiceGetAll()
        {
            var service = Kernel.Get<ProductDescriptionService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void ProductDescriptionServiceGetById()
        {
            var service = Kernel.Get<ProductDescriptionService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class ProductModelServiceTest : TestBase
    {
        [TestMethod]
        public void ProductModelServiceGetAll()
        {
            var service = Kernel.Get<ProductModelService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void ProductModelServiceGetById()
        {
            var service = Kernel.Get<ProductModelService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
    [TestClass]
    public partial class SalesOrderDetailServiceTest : TestBase
    {
        [TestMethod]
        public void SalesOrderDetailServiceGetAll()
        {
            var service = Kernel.Get<SalesOrderDetailService>();

            Assert.IsNotNull(service.GetAll());
        }

        [TestMethod]
        public void SalesOrderDetailServiceGetById()
        {
            var service = Kernel.Get<SalesOrderDetailService>();

            Assert.IsNotNull(service.GetById(1));
        }
    }
}