﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Solomon.Example.Services;
using System;
using Ninject;
using Solomon.Example.Dtos;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Reflection;
using Solomon.Example.DataModel;
using System.Data.Entity;
using System.Linq;
using Solomon.Example.OrmTool;

namespace Solomon.Example.Web.Test.Services
{
    public partial class CustomerServiceTest
    {
        

        [TestMethod]
        public void CustomerServiceSave()
        {
            var service = Kernel.Get<CustomerService>();

            var customer = new CustomerDto
            {
                CompanyName = "CompanyName",
                CustomerID = 1,
                EmailAddress = "EmailAddress",
                FirstName = "FirstName",
                LastName = "LastName",
                MiddleName = "MiddleName",
                ModifiedDate = DateTime.Now,
                NameStyle = false,
                PasswordHash = "PasswordHash",
                PasswordSalt = "PasswordSalt",
                Phone = "555-55-5555",
                rowguid = new Guid(),
                SalesPerson = "SalesPerson",
                Suffix = "Suffix",
                Title = "Title"
            };

            Assert.IsTrue(service.Save(customer));
        }

        [TestMethod]
        public void TestAdoData()
        {
            var customerAddressList = DbHelper.GetObjectSet<CustomerAddress>("select * from [SalesLT].[CustomerAddress]");

            Assert.IsNotNull(customerAddressList);
        }

        [TestMethod]
        public void TestEfData()
        {
            using (AdventureWorksEntities db = new AdventureWorksEntities())
            {
                var customerAddressList = db.CustomerAddresses.Where(x => x.CustomerID > 0);

                Assert.IsNotNull(customerAddressList);

            }
        }
    }

    public partial class CustomerAddress
    {
        public int CustomerID { get; set; }
        public int AddressID { get; set; }
        public string AddressType { get; set; }
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
