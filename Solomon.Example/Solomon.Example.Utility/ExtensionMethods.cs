﻿using Solomon.Common.Utility;
using System;
using System.Reflection;

namespace Solomon.Example.Utility
{
    public static class ExtensionMethods
    {
        public static string ToStringValue(this Enum value)
        {
            string output = null;
            Type type = value.GetType();

            FieldInfo fieldInfo = type.GetField(value.ToString());

            StringValueAttribute[] attributes = fieldInfo.GetCustomAttributes(typeof(StringValueAttribute), false) as StringValueAttribute[];
            if (attributes.Length > 0)
            {
                output = attributes[0].Value;
            }

            return output;
        }        
    }
}
