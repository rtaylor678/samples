﻿using Solomon.Example.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Solomon.Example.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void ExecuteCore()
        {
            SetCultureInfo();
            base.ExecuteCore();
        }

        protected override void OnAuthorization(AuthorizationContext authorizationContext)
        {
            if (authorizationContext == null)
            {
                throw new ArgumentNullException("authorizationContext");
            }

            //enforce anti-forgery stuff for HttpVerbs.Post
            if (String.Compare(authorizationContext.HttpContext.Request.HttpMethod, System.Net.WebRequestMethods.Http.Post, true) == 0)
            {
                var forgery = new ValidateAntiForgeryTokenAttribute();
                forgery.OnAuthorization(authorizationContext);
            }


            //Check if user is Authenticated or not
            if (!Request.IsAuthenticated)
            {
                authorizationContext.Result = RedirectToAction("Login", "Account");
                return;
            }

            // Check if current user has access to controller
            var roleAttributesController = authorizationContext.Controller.GetType().GetCustomAttributes(typeof(AuthorizeRolesAttribute), true);

            if (roleAttributesController.Length != 0)
            {
                //Get it from Database
                string[] userRoles = { "User", "Admin" };
                //string[] userRoles = { "User" };

                if (!HasAuthorization(roleAttributesController, userRoles))
                {
                    authorizationContext.Result = RedirectToAction("Login", "Account");
                    return;
                }    
            }

            // Check if current user has access to specific action of the controller
            var roleAttributesAction = authorizationContext.ActionDescriptor.GetCustomAttributes(typeof(AuthorizeRolesAttribute), true);

            if (roleAttributesAction.Length != 0)
            {
                //Get it from Database
                string[] userRoles = { "Admin", "User" };
                //string[] userRoles = { "User" };

                if (!HasAuthorization(roleAttributesAction, userRoles))
                {
                    authorizationContext.Result = RedirectToAction("Login", "Account");
                    return;
                }
            }
        }

        protected override void EndExecuteCore(IAsyncResult asyncResult)
        {
            SetCultureInfo();
            base.EndExecuteCore(asyncResult);
        }


        private void SetCultureInfo()
        {
            // Validate culture name
            //string cultureName = (string)Session["Language"];
            string cultureName = "es";

            if (!string.IsNullOrWhiteSpace(cultureName))
            {
                // Modify current thread's cultures            
                Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }

        private bool HasAuthorization(object[] authorizeRoles, string[] userRoles)
        {
            bool hasAuthorization = false;

            foreach (AuthorizeRolesAttribute item in authorizeRoles)
            {                
                var authorizedRoles = item.Roles.Split(',').Select(role => role.Trim()).ToList();
                foreach (var role in userRoles)
                {
                    if (authorizedRoles.Contains(role))
                    {
                        hasAuthorization = true;
                        break;
                    }
                }
            }

            return hasAuthorization;
        }
    }
}
