﻿using AutoMapper;
using Solomon.Example.Dtos;
using Solomon.Example.Services.Interfaces;
using Solomon.Example.Web.Helpers;
using Solomon.Example.Web.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Solomon.Example.Web.Controllers
{
    [AuthorizeRoles(Role.Admin, Role.User)]
    public class CustomerController : BaseController
    {
        ICustomerService _customerService;        

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public ICustomerService CustomerService
        {
            get
            {
                return _customerService;
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [AuthorizeRoles(Role.Admin)]        
        public ActionResult AllCustomers()
        {
            var customers = new CustomerListDto { Customers = CustomerService.GetAll() };

            return View("CustomerList", customers);
        }

        [AuthorizeRoles(Role.Admin)]
        public ActionResult Save(CustomerDto customer)
        {
            customer.CustomerID = 29485;

            CustomerService.Save(customer);

            return View("Index", customer);
        }
    }
}
