[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Solomon.Example.Web.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Solomon.Example.Web.App_Start.NinjectWebCommon), "Stop")]

namespace Solomon.Example.Web.App_Start
{
    using System;
    using System.Web;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;
    using Ninject.Extensions.Conventions;
    using Ninject;
    using Ninject.Web.Common;
    using Solomon.Example.DataModel;
    using System.Data.Entity;
    using Solomon.Common.Repository;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //kernel.Bind<ICustomerService>().To<CustomerService>();
            //kernel.Scan(scanner =>
            //{
            //    // look for types in this assembly
            //    scanner.FromCallingAssembly();

            //    // make ISomeType bind to SomeType by default (remove the 'I'!)
            //    scanner.BindWith<DefaultBindingGenerator>();
            //});


            //kernel.Bind(x =>
            //{
            //    x.FromThisAssembly() // Scans currently assembly
            //     .SelectAllClasses() // Retrieve all non-abstract classes
            //     .BindDefaultInterface(); // Binds the default interface to them;
            //});

            //kernel.Bind(x =>
            //{
            //    x.FromAssembliesInPath("") // Scans currently assembly
            //     .SelectAllClasses() // Retrieve all non-abstract classes
            //     .BindDefaultInterface(); // Binds the default interface to them;
            //});
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>();
            kernel.Bind<DbContext>().To<AdventureWorksEntities>();
            
            kernel.Bind(x =>
            {
                x.FromAssemblyContaining<Solomon.Example.Services.CustomerService>()
                 .SelectAllClasses()
                 .BindDefaultInterface();
            });

            kernel.Bind(x =>
            {
                x.FromAssemblyContaining<Solomon.Example.Components.CustomerManager>()
                 .SelectAllClasses()
                 .BindDefaultInterface();
            });   
        }        
    }
}
