﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Solomon.Example.Web.Helpers
{
    public class ErrorLoggingUtility
    {
        public static void GetExceptionLogged(Exception exception)
        {
            StringBuilder sb = new StringBuilder();
            GetException(sb, exception);
            log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            log.Info("");
            log.Info(System.DateTime.Now.ToString());
            log.Error(sb.ToString());
        }

        public static void GetException(StringBuilder message, Exception exp)
        {
            if (exp == null)
            {
                return;
            }
            message.AppendFormat("Message: {0}\r\nStackTrace\r\n{1}\r\n\r\n", exp.Message, exp.StackTrace);
            GetException(message, exp.InnerException);
        }
    }
}