﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Solomon.Example.Web.Helpers
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
                {
                    x.AddProfile(new AutoMapperWebProfile());
                });
        }
    }

    public class AutoMapperWebProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();
            //Create maps here
        }
    }
}