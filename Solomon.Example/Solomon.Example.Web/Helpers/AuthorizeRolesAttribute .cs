﻿using Solomon.Common.Utility;
using System.Collections.Generic;
using System.Web.Http;


namespace Solomon.Example.Web.Helpers
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params Role[] roles ) : base()
        {
            List<string> authorizeRoles = new List<string>();
            
            foreach (var role in roles)
            {
                authorizeRoles.Add(role.ToString());
            }
            Roles = string.Join(",", authorizeRoles);
        }
        //public string AuthorizedRules
        //{
        //    get { return Roles; }
        //}
    }

    public enum Role
    {
        [StringValue("Admin")]
        Admin = 1,
        [StringValue("User")]
        User = 2
    }
}