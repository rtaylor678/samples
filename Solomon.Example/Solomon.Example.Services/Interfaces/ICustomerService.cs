﻿using Solomon.Example.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Example.Services.Interfaces
{
    public interface ICustomerService : ICustomerServiceBase
    {
        bool Save(CustomerDto customer);
    }
}
