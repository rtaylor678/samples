﻿using AutoMapper;
using Solomon.Common.Repository;
using Solomon.Example.Components.Interfaces;
using Solomon.Example.DataModel;
using Solomon.Example.Dtos;
using Solomon.Example.Services.Interfaces;

namespace Solomon.Example.Services
{
    public partial class CustomerService : ICustomerService
    {
        ICustomerManager _customerManager;
        IAddressManager _addressManager;

        public CustomerService(
            IUnitOfWork uow,
            ICustomerManager customerManager,
            IAddressManager addressManager)
            : base(uow)
        {
            _customerManager = customerManager;
            _addressManager = addressManager;
        }

        public bool Save(CustomerDto customer)
        {
            _customerManager.Update(Mapper.Map<Customer>(customer));

            _addressManager.GetById(1);

            return Uow.SaveChanges();
        }

        public void ProcessCustomerData(CustomerDto customerDto)
        {
            _customerManager.Update(Mapper.Map<Customer>(customerDto));

            _addressManager.GetById(2);

            //Do something with address

            Uow.SaveChanges();            
        }        
    }
}
