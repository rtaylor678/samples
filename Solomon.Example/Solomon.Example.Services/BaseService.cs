﻿
using Solomon.Common.Repository;
namespace Solomon.Example.Services
{
    public class BaseService
    {
        IUnitOfWork _uow;
        public BaseService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public BaseService() { }

        public IUnitOfWork Uow
        {
            get { return _uow; }            
        }
    }
}
