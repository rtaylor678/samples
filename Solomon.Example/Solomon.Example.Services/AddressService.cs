﻿using Solomon.Common.Repository;
using Solomon.Example.Components.Interfaces;
using Solomon.Example.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Example.Services
{
    public partial class AddressService : IAddressService
    {
        IAddressManager _addressManager;

        public AddressService(IUnitOfWork uow, IAddressManager addressManager) : base(uow)
        {
            _addressManager = addressManager;
        }
    }
}
