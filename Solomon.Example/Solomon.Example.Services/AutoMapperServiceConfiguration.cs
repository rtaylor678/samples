﻿using AutoMapper;
using Solomon.Example.DataModel;
using Solomon.Example.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Example.Services
{
    public static class AutoMapperServiceConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(x =>
                {
                    x.AddProfile(new AutoMapperServiceProfile());                
                });
        }
    }

    public class AutoMapperServiceProfile : Profile
    {
        protected override void Configure()
        {
            base.Configure();
            Mapper.CreateMap<CustomerDto, Customer>();
            Mapper.CreateMap<Customer, CustomerDto>();

            Mapper.CreateMap<AddressDto, Address>();
            Mapper.CreateMap<Address, AddressDto>();

        }
    }

}
