﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Example.Dtos
{
    public class CustomerListDto
    {
        public List<CustomerDto> Customers { get; set; }
    }
}
