﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solomon.Example.Dtos
{
    public class CustomerAddressDto
    {
        public int CustomerID { get; set; }
        public int AddressID { get; set; }
        public string AddressType { get; set; }
        
        public virtual AddressDto Address { get; set; }
        public virtual CustomerDto Customer { get; set; }
    }
}
