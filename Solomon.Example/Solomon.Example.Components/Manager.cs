﻿
using Solomon.Common.Repository;
namespace Solomon.Example.Components
{
    public class Manager<TEntity> where TEntity : class
    {
        public IRepository<TEntity> Repository 
        { 
            get; 
            private set; 
        }

        //public Manager()
        //{
        //    this.Repository = new UnitOfWork().GetRepository<TEntity>();
        //}

        public Manager(IUnitOfWork uow)
        {
            this.Repository = uow.GetRepository<TEntity>();
        }
    }
}
