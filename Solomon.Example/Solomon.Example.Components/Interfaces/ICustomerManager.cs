﻿using Solomon.Example.DataModel;
using System.Collections.Generic;

namespace Solomon.Example.Components.Interfaces
{
    public interface ICustomerManager : ICustomerManagerBase
    {
        void Update(Customer customer);
        bool Login(string userName, string password, bool persistCookies = false);
    }
}
