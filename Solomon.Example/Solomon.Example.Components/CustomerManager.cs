﻿using Solomon.Example.DataModel;
using Solomon.Example.Components.Interfaces;

namespace Solomon.Example.Components
{
    public partial class CustomerManager : ICustomerManager
    {
        public void Update(Customer customer)
        {
            var persistedCustomer = GetById(customer.CustomerID);

            //Change properties
            persistedCustomer.CompanyName = customer.CompanyName;
            persistedCustomer.EmailAddress = customer.EmailAddress;
            persistedCustomer.FirstName = customer.FirstName;
            persistedCustomer.LastName = customer.LastName;
            persistedCustomer.SalesPerson = customer.SalesPerson;
            
            Repository.Update(persistedCustomer);
        }

        public bool Login(string userName, string password, bool persistCookies = false)
        {
            return Repository.GetCount(x => x.EmailAddress == userName && x.PasswordHash == password) > 0;
        }
    }
}
