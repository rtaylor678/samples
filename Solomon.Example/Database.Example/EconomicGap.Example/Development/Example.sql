﻿/*
The 'Flow', having more than the target is good (+) or less than the target is good (-) may be able to come from a LookUp (Parent) table.
*/

SELECT
	[t].*,
	[g].*
FROM (VALUES
	(1, 5, 10, 6, 17),
	(1, 6, 7, 8, 9),
	(1, 6, 7, 7, 6)
	) [t] ([Flow], [TargetQuantity], [TargetRate], [AuditQuantity], [AuditRate])
CROSS APPLY
	[gap].[Gap]([t].[Flow], [t].[TargetQuantity], [t].[TargetRate], [t].[AuditQuantity], [t].[AuditRate])	[g]

	