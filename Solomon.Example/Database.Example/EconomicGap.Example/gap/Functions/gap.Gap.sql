﻿CREATE FUNCTION [gap].[Gap]
(
	@Flow				INT,	--	+1 for Revenue	(Audit > Target is better)
								--	-1 for Expenses	(Audit < Target is better)
	@TargetQuantity		FLOAT,
	@TargetRate			FLOAT,

	@AuditQuantity		FLOAT,
	@AuditRate			FLOAT
)
RETURNS TABLE
WITH SCHEMABINDING
AS
RETURN
(
	SELECT
		[g].[Target],
		[g].[Audit],
		[g].[Common],

		[g].[GapQuantity],
		[g].[GapRate],
		[g].[GapMix],
		[g].[GapTotal],

			[Gap_Quantity_Pcnt]	=  CONVERT(FLOAT, [g].[GapQuantity]		/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix])) * 100.0),
			[Gap_Rate_Pcnt]		=  CONVERT(FLOAT, [g].[GapRate]			/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix])) * 100.0),
			[Gap_Mix_Pcnt]		=  CONVERT(FLOAT, [g].[GapMix]			/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix])) * 100.0),

			[Common_Pcnt]		=  CONVERT(FLOAT, [g].[Common]			/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix]) + ABS([g].[Common])) * 100.0),
			[Quantity_Pcnt]		=  CONVERT(FLOAT, [g].[GapQuantity]		/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix]) + ABS([g].[Common])) * 100.0),
			[Rate_Pcnt]			=  CONVERT(FLOAT, [g].[GapRate]			/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix]) + ABS([g].[Common])) * 100.0),
			[Mix_Pcnt]			=  CONVERT(FLOAT, [g].[GapMix]			/ (ABS([g].[GapRate]) + ABS([g].[GapQuantity]) + ABS([g].[GapMix]) + ABS([g].[Common])) * 100.0),

		[g].[Target_Geom],
		[g].[Audit_Geom],
		[g].[Common_Geom],

		[g].[GapQuantity_Geom],
		[g].[GapRate_Geom],
		[g].[GapMix_Geom]
	FROM (
		SELECT
			[g].[Target],
			[g].[Audit],
			[g].[Common],

			[g].[GapQuantity],
			[g].[GapRate],
				[GapMix]		=  CONVERT(FLOAT, [g].[GapTotal] - [g].[GapQuantity] - [g].[GapRate]),
			[g].[GapTotal],

			[g].[Target_Geom],
			[g].[Audit_Geom],
			[g].[Common_Geom],
			[g].[GapQuantity_Geom],
			[g].[GapRate_Geom],
			[g].[GapMix_Geom]
		FROM (
			SELECT
				[Target]		= CONVERT(FLOAT, @TargetQuantity	* @TargetRate),
				[Audit]			= CONVERT(FLOAT, @AuditQuantity		* @AuditRate),
				[Common]		= CONVERT(FLOAT,
									CASE WHEN @TargetQuantity	< @AuditQuantity	THEN @TargetQuantity	ELSE @AuditQuantity END *
									CASE WHEN @TargetRate		< @AuditRate		THEN @TargetRate		ELSE @AuditRate		END),

				[GapQuantity]	= CONVERT(FLOAT, @Flow * CASE WHEN @TargetRate		< @AuditRate		THEN @TargetRate		ELSE @AuditRate		END * (@AuditQuantity	- @TargetQuantity)),
				[GapRate]		= CONVERT(FLOAT, @Flow * CASE WHEN @TargetQuantity	< @AuditQuantity	THEN @TargetQuantity	ELSE @AuditQuantity END * (@AuditRate		- @TargetRate)),
				[GapTotal]		= CONVERT(FLOAT, @Flow * ((@AuditRate * @AuditQuantity) - (@TargetRate * @TargetQuantity))),


				[Target_Geom]	= sys.geometry::STPolyFromText('POLYGON((' 
							+ '0.0'								+ ' ' + '0.0'							+ ','
							+ CAST(@TargetQuantity AS VARCHAR)	+ ' ' + '0.0'							+ ','
							+ CAST(@TargetQuantity AS VARCHAR)	+ ' ' + CAST(@TargetRate AS VARCHAR)	+ ','
							+ '0.0'								+ ' ' + CAST(@TargetRate AS VARCHAR)	+ ','
							+ '0.0'								+ ' ' + '0.0'							+ '))', 0),

				[Audit_Geom]	= sys.geometry::STPolyFromText('POLYGON((' 
							+ '0.0'								+ ' ' + '0.0'							+ ','
							+ CAST(@AuditQuantity AS VARCHAR)	+ ' ' + '0.0'							+ ','
							+ CAST(@AuditQuantity AS VARCHAR)	+ ' ' + CAST(@AuditRate AS VARCHAR)		+ ','
							+ '0.0'								+ ' ' + CAST(@AuditRate AS VARCHAR)		+ ','
							+ '0.0'								+ ' ' + '0.0'							+ '))', 0),

				[Common_Geom]	= sys.geometry::STPolyFromText('POLYGON((' 
							+ '0.0'																		+ ' ' + '0.0'																														+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + '0.0'																						+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ '0.0'																										+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ '0.0'																		+ ' ' + '0.0'																														+ '))', 0),

				[GapQuantity_Geom]	= sys.geometry::STPolyFromText('POLYGON((' 
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + '0.0'																		+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @AuditQuantity ELSE @TargetQuantity END AS VARCHAR)	+ ' ' + '0.0'																		+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @AuditQuantity ELSE @TargetQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + '0.0'																		+ '))', 0),

				[GapRate_Geom]	= sys.geometry::STPolyFromText('POLYGON((' 
							+ '0.0'																			+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @AuditRate ELSE @TargetRate END AS VARCHAR)	+ ','
							+ '0.0'																			+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @AuditRate ELSE @TargetRate END AS VARCHAR)	+ ','
							+ '0.0'																			+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ '))', 0),

				[GapMix_Geom]	= CASE WHEN ((@TargetRate > @AuditRate AND @TargetQuantity > @AuditQuantity) OR (@TargetRate < @AuditRate AND @TargetQuantity < @AuditQuantity))
						THEN sys.geometry::STPolyFromText('POLYGON((' 
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @AuditQuantity ELSE @TargetQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @AuditQuantity ELSE @TargetQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @AuditRate ELSE @TargetRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @AuditRate ELSE @TargetRate END AS VARCHAR)	+ ','
							+ CAST(CASE WHEN @TargetQuantity < @AuditQuantity THEN @TargetQuantity ELSE @AuditQuantity END AS VARCHAR)	+ ' ' + CAST(CASE WHEN @TargetRate < @AuditRate THEN @TargetRate ELSE @AuditRate END AS VARCHAR)	+'))', 0)
						END
			) [g]
		) [g]
);