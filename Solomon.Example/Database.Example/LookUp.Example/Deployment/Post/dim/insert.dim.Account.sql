﻿PRINT 'insert.dim.Account...';

DECLARE @Accounts TABLE
(
	[AC_Tag]			VARCHAR(24)			NOT	NULL	CHECK([AC_Tag] <> ''),
														UNIQUE CLUSTERED([AC_Tag] ASC),

	[AC_Name]			NVARCHAR(48)		NOT	NULL	CHECK([AC_Name] <> '')
														UNIQUE([AC_Name] ASC),

	[AC_Detail]			NVARCHAR(96)		NOT	NULL	CHECK([AC_Detail] <> '')
														UNIQUE([AC_Detail] ASC),

	[AC_Operator]		CHAR(1)				NOT	NULL	CHECK([AC_Operator] <> ''),

	[AC_Parent]			VARCHAR(24)			NOT	NULL	CHECK([AC_Parent] <> '')
);

INSERT INTO @Accounts([AC_Tag], [AC_Name], [AC_Detail], [AC_Operator], [AC_Parent])
SELECT [t].[AC_Tag], [t].[AC_Name], [t].[AC_Detail], [t].[AC_Operator], [t].[AC_Parent]
FROM (VALUES
	('BAL', 'Balance', 'Balance', '+', 'BAL'),

	('EXP', 'Expenses', 'Expenses', '-', 'BAL'),
		('SWB', 'Salaries', 'Salaries, Wages, and Benefits', '-', 'EXP'),
		('MNT', 'Maintenance', 'Maintenance', '-', 'EXP'),
		('TAX', 'Taxes', 'Taxes', '-', 'EXP'),
		('UTIL', 'Utilities', 'Utilities', '-', 'EXP'),
			('ELEC', 'Electric', 'Electric', '-', 'UTIL'),
			('STM', 'Steam', 'Steam', '-', 'UTIL'),
			('COL', 'Cooling', 'Cooling Water', '-', 'UTIL'),
		
	('INC', 'Income', 'Income', '+', 'BAL'),
		('Sales', 'Sales', 'Sales', '+', 'INC'),
		('IP', 'IP Revenue', 'IP Revenue', '+', 'INC')

	)[t]([AC_Tag], [AC_Name], [AC_Detail], [AC_Operator], [AC_Parent]);

INSERT INTO [dim].[AC_Account_LookUp]
(
	[AC_Tag],
	[AC_Name],
	[AC_Detail]
)
SELECT
	[t].[AC_Tag],
	[t].[AC_Name],
	[t].[AC_Detail]
FROM
	@Accounts					[t]
LEFT OUTER JOIN
	[dim].[AC_Account_LookUp]	[x]
		ON	[x].[AC_Tag]	= [t].[AC_Tag]
WHERE
	[x].[AC_AccountId]		IS NULL;

INSERT INTO [dim].[AC_Account_Parent]
(
	[AC_MethodologyId],
	[AC_AccountId],
	[AC_ParentId],
	[AC_Operator],
	[AC_SortKey],
	[AC_Hierarchy]
)
SELECT
		[AC_MethodologyId]	= 0,
	[i].[AC_AccountId],
	[p].[AC_AccountId],
	[t].[AC_Operator],
		[AC_SortKey]		= 0,
		[AC_Hierarchy]		= '/'
FROM
	@Accounts							[t]
INNER JOIN
	[dim].[AC_Account_LookUp]			[i]
		ON	[i].[AC_Tag]	= [t].[AC_Tag]
INNER JOIN
	[dim].[AC_Account_LookUp]			[p]
		ON	[p].[AC_Tag]	= [t].[AC_Parent]
LEFT OUTER JOIN
	[dim].[AC_Account_Parent]			[x]
		ON	[x].[AC_AccountId]	= [i].[AC_AccountId]
		AND	[x].[AC_ParentId]	= [p].[AC_AccountId]
WHERE
	[x].[AC_AccountId]		IS NULL;

EXECUTE [dim].[Update_Parent] 'dim', 'AC_Account_Parent', 'AC', 'AccountId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;
EXECUTE [dim].[Merge_Bridge] 'dim', 'AC_Account_Parent', 'dim', 'AC_Account_Bridge', 'AC', 'AccountId', DEFAULT, DEFAULT, DEFAULT, DEFAULT;