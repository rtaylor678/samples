﻿PRINT 'insert.fact.Population...';

DECLARE @Population TABLE
(
	[PO_Tag]				VARCHAR(24)			NOT	NULL	CHECK([PO_Tag] <> ''),
															UNIQUE CLUSTERED([PO_Tag] ASC),

	[PO_Population_Count]	FLOAT				NOT	NULL	CHECK([PO_Population_Count] > 0)
);

INSERT INTO @Population([PO_Tag], [PO_Population_Count])
SELECT [t].[PO_Tag], [t].[PO_Population_Count]
FROM (VALUES
	('NA', NULL),
	('CAN', 35.16),
	('USA', 316.5),
	
	('USA-TX', 26.96),
	('USA-TX-DAL', 1.258),
	('USA-TX-HOU', 2.196),

	('USA-CA', 38.8),
	('USA-CA-LA', 18.55),
	('USA-CA-ES', 0.16924)
	) [t] ([PO_Tag], [PO_Population_Count])
WHERE
	[t].[PO_Population_Count]	IS NOT NULL;

INSERT INTO [fact].[PO_Population]
(
	[PO_LocationId],
	[PO_Population_Count]
)
SELECT
	[l].[LO_LocationId],
	[p].[PO_Population_Count]
FROM
	@Population					[p]
INNER JOIN
	[dim].[LO_Location_LookUp]	[l]
		ON	[l].[LO_Tag]		= [p].[PO_Tag]
LEFT OUTER JOIN
	[fact].[PO_Population]		[x]
		ON	[x].[PO_LocationId]	= [l].[LO_LocationId]
WHERE	[x].[PO_LocationId]	IS NULL
	AND	[l].[LO_Tag] IN ('CAN', 'USA-TX-DAL', 'USA-TX-HOU', 'USA-CA-LA', 'USA-CA-ES');