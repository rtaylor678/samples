﻿PRINT 'insert.fact.BalanceSheet...';

DECLARE @BalanceSheet TABLE
(
	[BA_Tag]				VARCHAR(24)			NOT	NULL	CHECK([BA_Tag] <> ''),
															UNIQUE CLUSTERED([BA_Tag] ASC),

	[BA_Amount_USD]			FLOAT				NOT	NULL	CHECK([BA_Amount_USD] > 0)
);

INSERT INTO @BalanceSheet([BA_Tag], [BA_Amount_USD])
SELECT [t].[BA_Tag], [t].[BA_Amount_USD]
FROM (VALUES
	('SWB', 22.4),

	('MNT', 5.7),
	('TAX', 2.8),
	
	('ELEC', 9.3),
	('STM', 7.8),
	('COL', 1.2),

	('Sales', 38.8),
	('IP', 16.9)
	) [t] ([BA_Tag], [BA_Amount_USD])
WHERE
	[t].[BA_Amount_USD]	IS NOT NULL;

INSERT INTO [fact].[BA_BalanceSheet]
(
	[BA_AccountId],
	[BA_Amount_USD]
)
SELECT
	[l].[AC_AccountId],
	[b].[BA_Amount_USD]
FROM
	@BalanceSheet				[b]
INNER JOIN
	[dim].[AC_Account_LookUp]	[l]
		ON	[l].[AC_Tag]		= [b].[BA_Tag]
LEFT OUTER JOIN
	[fact].[BA_BalanceSheet]	[x]
		ON	[x].BA_AccountId	= [l].[AC_AccountId]
WHERE
	[x].[BA_AccountId]	IS NULL;