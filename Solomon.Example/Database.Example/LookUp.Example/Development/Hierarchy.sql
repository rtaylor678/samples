﻿SELECT
	REPLICATE(' ', POWER([p].[AC_Hierarchy].GetLevel() - 1, 2)) + [l].[AC_Tag],
	[l].[AC_Tag],
	[l].[AC_Name],
	[l].[AC_Detail],
	[p].[AC_Hierarchy].GetLevel(),
	[p].[AC_Hierarchy].ToString(),
	[p].[AC_Operator]
FROM [dim].[AC_Account_Parent]			[p]
INNER JOIN [dim].[AC_Account_LookUp]	[l]
	ON	[l].[AC_AccountId] = [p].[AC_AccountId]
ORDER BY [p].[AC_Hierarchy];


SELECT
	REPLICATE(' ', POWER([p].[LO_Hierarchy].GetLevel() - 1, 2)) + [l].[LO_Tag],
	[l].[LO_Tag],
	[l].[LO_Name],
	[l].[LO_Detail],
	[p].[LO_Hierarchy].GetLevel(),
	[p].[LO_Hierarchy].ToString(),
	[p].[LO_Operator]
FROM [dim].[LO_Location_Parent]			[p]
INNER JOIN [dim].[LO_Location_LookUp]	[l]
	ON	[l].[LO_LocationId] = [p].[LO_LocationId]
ORDER BY [p].[LO_Hierarchy];
