﻿/*
Tables with the alias [p] or [l] are used for displaying the Tag for end users; this should be done in the UI.
*/

SELECT
	[b].[LO_LocationId],
		[LO_Tag]				= REPLICATE(' ', POWER([p].[LO_Hierarchy].GetLevel() - 1, 2)) + [l].[LO_Tag],
		[PO_Population_Count]	= SUM(CASE [m].[LO_Operator] 
									WHEN '+'	THEN   [x].[PO_Population_Count]
									WHEN '-'	THEN - [x].[PO_Population_Count]
									END),
		[Items]	= COUNT(1)
FROM
	[dim].[LO_Location_Bridge]	[b]
INNER JOIN
	[dim].[LO_Location_Parent]	[m]
		ON	[m].[LO_LocationId]	= [b].[LO_DescendantId]
INNER JOIN
	[fact].[PO_Population]		[x]
		ON	[x].[PO_LocationId]	= [b].[LO_DescendantId]

INNER JOIN
	[dim].[LO_Location_Parent]	[p]
		ON	[p].[LO_LocationId]	= [b].[LO_LocationId]
INNER JOIN
	[dim].[LO_Location_LookUp]	[l]
		ON	[l].[LO_LocationId]	= [b].[LO_LocationId]
GROUP BY
	[b].[LO_LocationId],
	[l].[LO_Tag],
	[p].[LO_Hierarchy]
ORDER BY
	[p].[LO_Hierarchy] ASC;


SELECT
	[b].[AC_AccountId],
		[AC_Tag]				= REPLICATE(' ', POWER([p].[AC_Hierarchy].GetLevel() - 1, 2)) + [l].[AC_Tag],
		[BA_Amount_USD]			= SUM(CASE [m].[AC_Operator] 
									WHEN '+'	THEN   [x].[BA_Amount_USD]
									WHEN '-'	THEN - [x].[BA_Amount_USD]
									END),
		[Items]	= COUNT(1)
FROM
	[dim].[AC_Account_Bridge]	[b]
INNER JOIN
	[dim].[AC_Account_Parent]	[m]
		ON	[m].[AC_AccountId]	= [b].[AC_DescendantId]
INNER JOIN
	[fact].[BA_BalanceSheet]	[x]
		ON	[x].[BA_AccountId]	= [b].[AC_DescendantId]

INNER JOIN
	[dim].[AC_Account_Parent]	[p]
		ON	[p].[AC_AccountId]	= [b].[AC_AccountId]
INNER JOIN
	[dim].[AC_Account_LookUp]	[l]
		ON	[l].[AC_AccountId]	= [b].[AC_AccountId]

GROUP BY
	[b].[AC_AccountId],
	[l].[AC_Tag],
	[p].[AC_Hierarchy]
ORDER BY
	[p].[AC_Hierarchy] ASC;