﻿CREATE TABLE [fact].[BA_BalanceSheet]
(
	[BA_BalanceSheetId]		INT		NOT	NULL	IDENTITY(1, 1),

	[BA_AccountId]			INT		NOT	NULL	CONSTRAINT [FK__BA_BalanceSheet_AC_Account]			REFERENCES [dim].[AC_Account_LookUp]([AC_AccountId]),

	[BA_Amount_USD]			FLOAT	NOT	NULL	CONSTRAINT [CR__BA_BalanceSheet_BA_Amount_USD]		CHECK([BA_Amount_USD] >= 0),

	CONSTRAINT [PK__BA_BalanceSheet]	PRIMARY KEY NONCLUSTERED([BA_BalanceSheetId] ASC),
	CONSTRAINT [UK__BA_BalanceSheet]	UNIQUE CLUSTERED([BA_AccountId] ASC)
);
