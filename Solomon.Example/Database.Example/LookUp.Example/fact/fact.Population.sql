﻿CREATE TABLE [fact].[PO_Population]
(
	[PO_PopulationId]		INT		NOT	NULL	IDENTITY(1, 1),

	[PO_LocationId]			INT		NOT	NULL	CONSTRAINT [FK__PO_Population_LO_Location]			REFERENCES [dim].[LO_Location_LookUp]([LO_LocationId]),

	[PO_Population_Count]	FLOAT	NOT	NULL	CONSTRAINT [CR__PO_Population_PO_Population_Count]	CHECK([PO_Population_Count] >= 0),

	CONSTRAINT [PK__PO_Population]	PRIMARY KEY NONCLUSTERED([PO_PopulationId] ASC),
	CONSTRAINT [UK__PO_Population]	UNIQUE CLUSTERED([PO_LocationId] ASC)
);
