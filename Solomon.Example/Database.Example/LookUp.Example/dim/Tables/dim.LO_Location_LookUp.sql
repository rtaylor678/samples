﻿CREATE TABLE [dim].[LO_Location_LookUp]
(
	[LO_LocationId]			INT					NOT NULL	IDENTITY(1, 1),

	[LO_Tag]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__LO_Location_LookUp_LO_Tag]				CHECK([LO_Tag] <> ''),
															CONSTRAINT [UK__LO_Location_LookUp_LO_Tag]				UNIQUE CLUSTERED([LO_Tag] ASC),

	[LO_Name]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__LO_Location_LookUp_LO_Name]				CHECK([LO_Name] <> '')
															CONSTRAINT [UX__LO_Location_LookUp_LO_Name]				UNIQUE([LO_Name] ASC),

	[LO_Detail]				NVARCHAR(192)		NOT	NULL	CONSTRAINT [CL__LO_Location_LookUp_LO_Detail]			CHECK([LO_Detail] <> '')
															CONSTRAINT [UX__LO_Location_LookUp_LO_Detail]			UNIQUE([LO_Detail] ASC),

	CONSTRAINT [PK__LO_Location_LookUp]			PRIMARY KEY([LO_LocationId] ASC)
);