﻿CREATE TABLE [dim].[AC_Account_Bridge]
(
	[AC_MethodologyId]		INT					NOT	NULL,

	[AC_AccountId]			INT					NOT NULL	CONSTRAINT [FK__AC_Account_Bridge_AC_AccountId]						REFERENCES [dim].[AC_Account_LookUp]([AC_AccountId])
															CONSTRAINT [FK__AC_Account_Bridge_AC_Account_Parent_Ancestor]		FOREIGN KEY ([AC_MethodologyId], [AC_AccountId])
																																REFERENCES [dim].[AC_Account_Parent]([AC_MethodologyId], [AC_AccountId]),

	[AC_DescendantId]		INT					NOT NULL	CONSTRAINT [FK__AC_Account_Bridge_AC_DescendantId]					REFERENCES [dim].[AC_Account_LookUp]([AC_AccountId])
															CONSTRAINT [FK__AC_Account_Bridge_AC_Account_Parent_Descendant]		FOREIGN KEY ([AC_MethodologyId], [AC_AccountId])
																																REFERENCES [dim].[AC_Account_Parent]([AC_MethodologyId], [AC_AccountId]),
	
	CONSTRAINT [PK__AC_Account_Bridge]	PRIMARY KEY CLUSTERED([AC_MethodologyId] ASC, [AC_AccountId] ASC, [AC_DescendantId] ASC)
);