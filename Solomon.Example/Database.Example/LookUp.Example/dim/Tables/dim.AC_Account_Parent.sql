﻿CREATE TABLE [dim].[AC_Account_Parent]
(
	[AC_MethodologyId]		INT					NOT	NULL,

	[AC_AccountId]			INT					NOT NULL	CONSTRAINT [FK__AC_Account_Parent_AC_AccountId]			REFERENCES [dim].[AC_Account_LookUp]([AC_AccountId]),
	[AC_ParentId]			INT					NOT NULL	CONSTRAINT [FK__AC_Account_Parent_AC_Parent]			REFERENCES [dim].[AC_Account_LookUp]([AC_AccountId])
															CONSTRAINT [FK__AC_Account_Parent_AC_ParentParent]
															FOREIGN KEY ([AC_MethodologyId], [AC_AccountId])
															REFERENCES [dim].[AC_Account_Parent]([AC_MethodologyId], [AC_AccountId]),

	[AC_Operator]			CHAR(1)				NOT	NULL	CONSTRAINT [DF__AC_Account_Parent_AC_Operator]			DEFAULT('+')
															CONSTRAINT [FK__AC_Account_Parent_AC_Operator]			REFERENCES [dim].[OP_Operator_LookUp]([OP_OperatorId]),
	[AC_SortKey]			INT					NOT	NULL,
	[AC_Hierarchy]			SYS.HIERARCHYID		NOT	NULL,

	CONSTRAINT [PK__AC_Account_Parent]			PRIMARY KEY CLUSTERED ([AC_MethodologyId] DESC, [AC_AccountId] ASC)
);