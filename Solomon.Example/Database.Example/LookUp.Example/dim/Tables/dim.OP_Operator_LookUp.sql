﻿CREATE TABLE [dim].[OP_Operator_LookUp]
(
	[OP_OperatorId]			CHAR(1)				NOT	NULL	CONSTRAINT [CL__OP_Operator_LookUp_OP_OperatorId]		CHECK([OP_OperatorId] <> ''),

	CONSTRAINT [PK__OP_Operator_LookUp]			PRIMARY KEY CLUSTERED([OP_OperatorId])
);