﻿CREATE TABLE [dim].[AC_Account_LookUp]
(
	[AC_AccountId]			INT					NOT NULL	IDENTITY(1, 1),

	[AC_Tag]				VARCHAR(24)			NOT	NULL	CONSTRAINT [CL__AC_Account_LookUp_AC_Tag]				CHECK([AC_Tag] <> ''),
															CONSTRAINT [UK__AC_Account_LookUp_AC_Tag]				UNIQUE CLUSTERED([AC_Tag] ASC),

	[AC_Name]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL__AC_Account_LookUp_AC_Name]				CHECK([AC_Name] <> '')
															CONSTRAINT [UX__AC_Account_LookUp_AC_Name]				UNIQUE([AC_Name] ASC),

	[AC_Detail]				NVARCHAR(192)		NOT	NULL	CONSTRAINT [CL__AC_Account_LookUp_AC_Detail]			CHECK([AC_Detail] <> '')
															CONSTRAINT [UX__AC_Account_LookUp_AC_Detail]			UNIQUE([AC_Detail] ASC),

	CONSTRAINT [PK__AC_Account_LookUp]			PRIMARY KEY([AC_AccountId] ASC)
);