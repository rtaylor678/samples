﻿CREATE PROCEDURE [dim].[Merge_Bridge]
(
	@SourceSchemaName		VARCHAR(48)	= 'dim',
	@SourceTableName		VARCHAR(48),

	@TargetSchemaName		VARCHAR(48)	= 'dim',
	@TargetTableName		VARCHAR(48),

	@Prefix					VARCHAR(2),
	@NamePrime				VARCHAR(48),

	@NameMethodology		VARCHAR(48)	= 'MethodologyId',
	@NameParent				VARCHAR(48)	= 'ParentId',
	@NameSortKey			VARCHAR(48)	= 'SortKey',
	@NameHierarchy			VARCHAR(48)	= 'Hierarchy',
	@NameOperator			VARCHAR(48)	= 'Operator'

)
AS
BEGIN

SET NOCOUNT ON;

	SET @NamePrime			= @Prefix + '_' + @NamePrime;
	SET @NameMethodology	= @Prefix + '_' + @NameMethodology;
	SET @NameParent			= @Prefix + '_' + @NameParent;
	SET @NameSortKey		= @Prefix + '_' + @NameSortKey;
	SET @NameHierarchy		= @Prefix + '_' + @NameHierarchy;

	DECLARE @SQL	VARCHAR(MAX) = 
	'MERGE INTO [' + @TargetSchemaName + '].[' + @TargetTableName + '] AS Target
	USING
	(
		SELECT
			a.[' + @NameMethodology + '],
			a.[' + @NamePrime + '],
			d.[' + @NamePrime + ']			[' + @Prefix + '_DescendantId]
		FROM [' + @SourceSchemaName + '].[' + @SourceTableName + ']			a
		INNER JOIN [' + @SourceSchemaName + '].[' + @SourceTableName + ']	d
			ON	d.[' + @NameMethodology + '] = a.[' + @NameMethodology + ']
			AND	d.[' + @NameHierarchy + '].IsDescendantOf(a.[' + @NameHierarchy + ']) = 1
	)
	AS Source([' + @NameMethodology + '], [' + @NamePrime + '], [' + @Prefix + '_DescendantId])
	ON	Target.[' + @NameMethodology + '] = Source.[' + @NameMethodology + ']
	AND	Target.[' + @NamePrime + '] = Source.[' + @NamePrime + ']
	AND	Target.[' + @Prefix + '_DescendantId] = Source.[' + @Prefix + '_DescendantId]
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([' + @NameMethodology + '], [' + @NamePrime + '], [' + @Prefix + '_DescendantId])
		VALUES([' + @NameMethodology + '], [' + @NamePrime + '], [' + @Prefix + '_DescendantId])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;'

	EXECUTE(@SQL);

END;

/*
EXECUTE dim.Merge_Bridge 'dim', 'Stream_Parent', 'dim', 'Stream_Bridge', 'MethodologyId', 'StreamId', 'SortKey', 'Hierarchy', 'Operator';
*/