﻿CREATE PROCEDURE [posit].[Insert_Positor]
(
	@PositorNote	NVARCHAR(348) = NULL
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(261) = N'[' + OBJECT_SCHEMA_NAME(@@PROCID) + N'].[' + OBJECT_NAME(@@PROCID) + N']';
	SET @PositorNote = COALESCE(@PositorNote, N'Automatically created by: ' + @ProcedureDesc);

	BEGIN TRY

		IF NOT EXISTS(
			SELECT TOP 1 1
			FROM
				[posit].[PO_Positor]		[p]
			WHERE	[p].[PO_PositorApp]		= APP_NAME()
				AND	[p].[PO_PositorHost]	= HOST_NAME()
				AND	[p].[PO_PositorUID]		= SUSER_ID()
				)
		BEGIN

			INSERT INTO [posit].[PO_Positor]
			(
				[PO_PositorNote]
			)
			VALUES
			(
				@PositorNote
			);

		END;

	END TRY
	BEGIN CATCH
	END CATCH;

	RETURN [posit].[Get_PositorId]();

END;
GO