﻿CREATE VIEW [q].[TasksToRun]
WITH SCHEMABINDING
AS
SELECT
	[q].[TaskQueueId],
	[q].[TaskQueueObjectId],
		[QueuedTime]	= [q].[tsInserted]
FROM
	[q].[TaskQueue]	[q]
LEFT OUTER JOIN
	[q].[TaskStart]	[s]
		ON	[s].[TaskQueueId]	= [q].[TaskQueueId]
WHERE
	[s].[TaskStartId]	IS NULL;
