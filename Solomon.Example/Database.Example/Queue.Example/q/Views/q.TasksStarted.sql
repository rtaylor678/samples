﻿CREATE VIEW [q].[TasksStarted]
WITH SCHEMABINDING
AS
SELECT
	[q].[TaskQueueId],
	[q].[TaskQueueObjectId],
		[QueuedTime]	= [q].[tsInserted],
		[StartTime]		= [s].[tsInserted]
FROM
	[q].[TaskQueue]		[q]
INNER JOIN
	[q].[TaskStart]		[s]
		ON	[s].[TaskQueueId]	= [q].[TaskQueueId]
LEFT OUTER JOIN
	[q].[TaskComplete]	[c]
		ON	[c].[TaskStartId]	= [s].[TaskStartId]
WHERE
	[c].[TaskCompleteId]	IS NULL;