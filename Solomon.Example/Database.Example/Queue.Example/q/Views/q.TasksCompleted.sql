﻿CREATE VIEW [q].[TasksCompleted]
WITH SCHEMABINDING
AS
SELECT
	[q].[TaskQueueId],
	[q].[TaskQueueObjectId],
		[QueuedTime]		= [q].[tsInserted],
		[StartTime]			= [s].[tsInserted],
		[CompletedTime]		= [c].[tsInserted],
	[c].[TaskErrorCount],
		[Duration_Seconds]	= DATEDIFF(ss, [q].[tsInserted], [c].[tsInserted])
FROM
	[q].[TaskQueue]		[q]
INNER JOIN
	[q].[TaskStart]		[s]
		ON	[s].[TaskQueueId]	= [q].[TaskQueueId]
INNER JOIN
	[q].[TaskComplete]	[c]
		ON	[c].[TaskStartId]	= [s].[TaskStartId];