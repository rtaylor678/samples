﻿CREATE TABLE [q].[TaskQueue]
(
	[TaskQueueId]		INT						NOT	NULL	IDENTITY(1, 1),

	--	Include Columns to decribe the task to be preformed.
	[TaskQueueObjectId]	INT						NOT	NULL,

	[tsInserted]		DATETIMEOFFSET(7)		NOT	NULL	CONSTRAINT [DF_TaskQueue_tsInserted]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsInsertedApp]		NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskQueue_tsInsertedApp]			DEFAULT(APP_NAME()),
	[tsInsertedHost]	NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskQueue_tsInsertedHost]		DEFAULT(HOST_NAME()),
	[tsInsertedUSer]	NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskQueue_tsInsertedUser]		DEFAULT(SUSER_SNAME()),
	[tsRowGuid]			UNIQUEIDENTIFIER		NOT	NULL	CONSTRAINT [DF_TaskQueue_tsRowGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX_TaskQueue_tsRowGuid]				UNIQUE NONCLUSTERED([tsRowGuid]),

	CONSTRAINT [PK_TaskQueue]	PRIMARY KEY CLUSTERED([TaskQueueId] ASC)
);