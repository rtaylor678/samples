﻿CREATE TABLE [q].[TaskStart]
(
	[TaskStartId]		INT						NOT	NULL	IDENTITY(1, 1),
	[TaskQueueId]		INT						NOT	NULL	CONSTRAINT [FK_TaskStart_TaskQueue]				REFERENCES [q].[TaskQueue]([TaskQueueId]),

	-- Include Columns to decribe the starting status


	[tsInserted]		DATETIMEOFFSET(7)		NOT	NULL	CONSTRAINT [DF_TaskStart_tsInserted]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsInsertedApp]		NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskStart_tsInsertedApp]			DEFAULT(APP_NAME()),
	[tsInsertedHost]	NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskStart_tsInsertedHost]		DEFAULT(HOST_NAME()),
	[tsInsertedUSer]	NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskStart_tsInsertedUser]		DEFAULT(SUSER_SNAME()),
	[tsRowGuid]			UNIQUEIDENTIFIER		NOT	NULL	CONSTRAINT [DF_TaskStart_tsRowGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX_TaskStart_tsRowGuid]				UNIQUE NONCLUSTERED([tsRowGuid]),

	CONSTRAINT [PK_TaskStart]	PRIMARY KEY CLUSTERED([TaskStartId] ASC)
);