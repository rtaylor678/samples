﻿CREATE TABLE [q].[TaskComplete]
(
	[TaskCompleteId]	INT						NOT	NULL	IDENTITY(1, 1),
	[TaskStartId]		INT						NOT	NULL	CONSTRAINT [FK_TaskComplete_TaskStart]			REFERENCES [q].[TaskStart]([TaskStartId]),

	-- Include Columns to decribe the completed status
	[TaskErrorCount]	INT						NOT	NULL	CONSTRAINT [CR_TaskComplete_TaskErrorCount]		CHECK([TaskErrorCount] >= 0),

	[tsInserted]		DATETIMEOFFSET(7)		NOT	NULL	CONSTRAINT [DF_TaskComplete_tsInserted]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsInsertedApp]		NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskComplete_tsInsertedApp]		DEFAULT(APP_NAME()),
	[tsInsertedHost]	NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskComplete_tsInsertedHost]		DEFAULT(HOST_NAME()),
	[tsInsertedUSer]	NVARCHAR(128)			NOT	NULL	CONSTRAINT [DF_TaskComplete_tsInsertedUser]		DEFAULT(SUSER_SNAME()),
	[tsRowGuid]			UNIQUEIDENTIFIER		NOT	NULL	CONSTRAINT [DF_TaskComplete_tsRowGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,
															CONSTRAINT [UX_TaskComplete_tsRowGuid]			UNIQUE NONCLUSTERED([tsRowGuid]),

	CONSTRAINT [PK_TaskComplete]	PRIMARY KEY CLUSTERED([TaskCompleteId] ASC)
);